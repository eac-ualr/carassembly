
Assemble and Disassemble a Toy Engine

Versions Required For Building
Unity 4.6.7F1
Vuforia 5.0.5
Oculus 0.5.0

Project Organization
GearVR/		Unity Project for the Samsung Gear
Mobile/		Unity Project for Android Devices

Builds
Builds/GearVR.apk	Latest GearVR Build
Builds/Mobile.apk	Latest Android Build (Non-GearVR)

Note
Update APK Key at https://developer.vuforia.com/targetmanager/licenseManager/licenseListing