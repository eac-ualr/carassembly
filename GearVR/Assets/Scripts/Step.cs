﻿using UnityEngine;
using System.Collections;

public class Step : MonoBehaviour {
	public string objective;
	public string Objective {
		get {
			return objective;
		}
	}

	private bool playFlag = false;

	void Start() {
		playFlag = true;
	}

	void Update() {
		if (playFlag) {
			GetComponent<AudioSource>().Play();
		}

		playFlag = false;
	}

	void OnEnable() {
		if (!playFlag) {
			playFlag = true;
		}
	}

	void OnDisable() {
		if (playFlag) {
			playFlag = false;
		}
	}
}
