﻿using UnityEngine;
using System;
using System.Collections;


public class GearInput: MonoBehaviour {
	public GameObject StepManager;
	private StepManager sm;

	void Start() {
		OVRTouchpad.Create ();
		OVRTouchpad.TouchHandler += HandleTouchHandler;

		sm = (StepManager) StepManager.GetComponent(typeof(StepManager));
	}

	void FixedUpdate() {
		/*
		 * Mouse X
		 * Mouse Y
		 *
		 * Input.GetKeyDown(KeyCode.ESCAPE)
		 */

		if (Input.GetKeyDown(KeyCode.Escape)) {
			sm.NextStep();
		}

		OVRTouchpad.Update ();
	}

	void HandleTouchHandler (object sender, System.EventArgs e) {
		OVRTouchpad.TouchArgs touchArgs = (OVRTouchpad.TouchArgs) e;
		OVRTouchpad.TouchEvent touchEvent = touchArgs.TouchType;

		switch(touchEvent) {
			case OVRTouchpad.TouchEvent.SingleTap:
				sm.PreviousStep();
			break;

			case OVRTouchpad.TouchEvent.Left:
			break;

			case OVRTouchpad.TouchEvent.Right:
			break;

			case OVRTouchpad.TouchEvent.Up:
			break;

			case OVRTouchpad.TouchEvent.Down:
			break;
		}
	}
}
