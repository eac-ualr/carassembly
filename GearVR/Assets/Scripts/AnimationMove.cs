﻿using UnityEngine;
using System.Collections;

public class AnimationMove : MonoBehaviour {

	public GameObject pivot1;
	public GameObject pivot2;
	public float speed = 1.0f;

	private float t = 0.0f;

	void Update () {
		transform.position = Vector3.Lerp (pivot1.transform.position, pivot2.transform.position, t);

		t = (t <= 1) ? t + Time.deltaTime * speed : 0;
	}

	void OnDrawGizmosSelected() {
		Gizmos.color = Color.red;
		Gizmos.DrawSphere (pivot2.transform.position, 0.1f);

		Gizmos.color = Color.white;
		Gizmos.DrawSphere (pivot1.transform.position, 0.1f);
		Gizmos.DrawLine (pivot1.transform.position, pivot2.transform.position);
	}
}