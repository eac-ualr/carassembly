﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(AnimationRotate))]
public class AnimationRotateEditor : Editor {

    private AnimationRotate myTarget = null;

    void OnEnable() {
        myTarget = (AnimationRotate) target;
    }

    public override void OnInspectorGUI() {
        GUILayout.Label("Rotate around:");
        GUILayout.BeginHorizontal();
        GUILayout.Space(35);
        myTarget.RotateX = GUILayout.Toggle(myTarget.RotateX, " X");
        myTarget.RotateY = GUILayout.Toggle(myTarget.RotateY, " Y");
        myTarget.RotateZ = GUILayout.Toggle(myTarget.RotateZ, " Z");
        GUILayout.EndHorizontal();
        myTarget.speed = EditorGUILayout.FloatField("Rotation speed:", myTarget.speed);
    }

}
