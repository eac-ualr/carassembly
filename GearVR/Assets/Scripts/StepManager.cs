using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StepManager : MonoBehaviour {
    private static StepManager instance;
    public static StepManager Instance {
        get {
            if(instance == null)
                instance = GameObject.FindObjectOfType<StepManager>();
            return instance;
        }
    }

    public GameObject[] steps;
	public Text stepUIText;

    private int currentStep = 0;

    void Start() {
        if(steps.Length == 0)
            stepUIText.text = "No Steps to Follow";

        steps[0].SetActive(true); // Initial Step
        for(int i = 1; i < steps.Length; i++) {
            steps[i].SetActive(false);
        }

        DisplayStep();
    }

    public void NextStep() {
        steps[currentStep].SetActive(false);

        if(currentStep < steps.Length - 1)
            currentStep++;

        steps[currentStep].SetActive(true);

        if (steps[currentStep].tag == "Finish")
            DisplayFinish();
        else
            DisplayStep();
	}

    public void PreviousStep() {
        steps[currentStep].SetActive(false);

        if(currentStep > 0)
            currentStep--;

        steps[currentStep].SetActive(true);

        DisplayStep();
    }

    private void DisplayFinish() {
        stepUIText.color = new Color(0.35F, 0.93F, 0.05F, 1.0F);
        stepUIText.text = steps[currentStep].GetComponent<Step>().Objective;
    }

    private void DisplayStep() {
        stepUIText.color = new Color(1.0F, 1.0F, 1.0F, 1.0F);
        stepUIText.text = "Step " + (currentStep + 1) + ": " + steps[currentStep].GetComponent<Step>().Objective;
    }
}
