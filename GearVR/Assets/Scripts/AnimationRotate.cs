﻿using UnityEngine;
using System.Collections;

public class AnimationRotate : MonoBehaviour {

    public float speed = 100;

    [SerializeField]
    private bool rotateX = false;
    public bool RotateX { get { return rotateX; } set { rotateX = value; }}
    [SerializeField]
    private bool rotateY = false;
    public bool RotateY { get { return rotateY; } set { rotateY = value; }}
    [SerializeField]
    private bool rotateZ = false;
    public bool RotateZ { get { return rotateZ; } set { rotateZ = value; }}

    void Start() {
        Vector3 rotationVector = new Vector3(rotateX ? -360 : 0,
                                             rotateY ? -360 : 0,
                                             rotateZ ? -360 : 0);
        iTween.RotateBy(gameObject, iTween.Hash("amount", rotationVector, "loopType", "loop", "easeType", "linear", "speed", speed));
    }
}
